import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

public class crud {

    //*************** Constants ***************//

    // Admin Credentials
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "password";

    // Functions
    private static final List<String> FUNCTION_MAIN = Arrays.asList("Add", "View", "Edit", "Delete", "Quit");
    private static final List<String> FUNCTION_ADD = Arrays.asList("Add Student", "Add Subject", "Back to Main");
    private static final List<String> FUNCTION_VIEW = Arrays.asList("View Registered Students", "View Student Record", "View Course", "View Statistics", "View All Records", "Back to Main");
    private static final List<String> FUNCTION_DELETE = Arrays.asList("Delete Student", "Delete Subject", "Back to Main");

    // Constants for Courses & Subjects
    private static final int INVALID_INPUT_NUM = 9999;
    private static final int ARRAY_START_INDEX = 0;
    private static final int ARRAY_MAX_INDEX = INVALID_INPUT_NUM - 1;
    private static final int MAX_SUBJECT_PER_COURSE = 5;
    private static final List<String> COURSES = Arrays.asList("IT", "CS", "IS", "SE");
    private static final List<String> IT_SUBJECTS = Arrays.asList("Java Programming", "Hardware Servicing", "Networking", "HTML and CSS", "Operating System");
    private static final List<String> CS_SUBJECTS = Arrays.asList("Java Programming", "Computing", "Design Thinking", "Analytics", "Algorithms");
    private static final List<String> IS_SUBJECTS = Arrays.asList("System Analysis Design", "Computer Applications", "Design Thinking", "Networking", "E-Commerce");
    private static final List<String> SE_SUBJECTS = Arrays.asList("Java Programming", "Operating System", "Design Thinking", "System Analysis and Design", "Algorithms");
    private static final List<List<String>> SUBJECT_LIST = new ArrayList<>();

    //*************** Global Variables ***************//

    // Main Records = Array Structure of Student Records
    private static List<StudentRecord> mainStudentRecord = new ArrayList<>();

    //*************** Main Methods ***************//

    // main class Method
    public static void main(String[] args) {
        initialize();
        mainProcess();
    }

    // initialization of records and subject list
    private static void initialize() {
        SUBJECT_LIST.add(IT_SUBJECTS);
        SUBJECT_LIST.add(CS_SUBJECTS);
        SUBJECT_LIST.add(IS_SUBJECTS);
        SUBJECT_LIST.add(SE_SUBJECTS);
        mainStudentRecord = new ArrayList<>();
    }

    // main process
    private static void mainProcess() {
        System.out.println("Start School Record System");
        boolean quit = false;

        while (!quit) {
            switch (getInputFunction()) {
                case 0:
                    processAddFunc();
                    break;
                case 1:
                    processViewFunc();
                    break;
                case 2:
                    processEditFunc();
                    break;
                case 3:
                    processDeleteFunc();
                    break;
                case 4:
                    quit = true;
                    break;
            }
        }

        System.out.println("\nSchool Record System Ends");
    }

    //*************** Function Methods ***************//
    private static void processAddFunc() {
        switch (getInputAddFunction()) {
            case 0:
                addNewStudent();
                break;
            case 1:
                addSubject();
                break;
            case 2:
                return;
        }
    }

    private static void processViewFunc() {
        switch (getInputViewFunction()) {
            case 0:
                viewRegisteredStudents();
                break;
            case 1:
                viewStudentRecords();
                break;
            case 2:
                viewCourse();
                break;
            case 3:
                viewStatistics();
                break;
            case 4:
                viewAllRecords();
                break;
            case 5:
                return;
        }
    }

    private static void processEditFunc() {
        if (authenticate()) {
            editStudentRecord();
        } else {
            System.out.println("ERROR: Access Denied!");
        }
    }

    private static void processDeleteFunc() {
        switch (getInputDeleteFunction()) {
            case 0:
                deleteStudent();
                break;
            case 1:
                deleteSubject();
                break;
            case 2:
                return;
        }
    }

    //*************** Sub-Function Methods ***************//

    private static void addNewStudent() {
        System.out.println("Adding New Record...");
        StudentRecord newRecord = new StudentRecord();
        newRecord.name = getInputName();
        newRecord.id = generateId();
        newRecord.course = getInputCourse();
        newRecord.contactNumber = getInputContactNumber();
        newRecord.enrolled = false;
        mainStudentRecord.add(newRecord);
        System.out.println("New record added with student id " + newRecord.id);
    }

    private static void addSubject() {
        System.out.println("Adding Subject...");
        String inputStudentId = getInputStudentId();
        StudentRecord studentRecord = getStudentRecordById(inputStudentId);
        int subjectToAdd = getInputSubjectId(studentRecord.course);
        addSubjectToRecord(inputStudentId, subjectToAdd);
    }

    private static void addSubjectToRecord(String studentId, int subject) {
        for (StudentRecord studentRecord : mainStudentRecord) {
            if (studentRecord.id.equals(studentId)) {
                studentRecord.subject.add(subject);
                System.out.printf("Updated Subjects: [%s]\n", getSubjectFromStudentRecord(studentRecord));
            }
            if (studentRecord.subject.size() >= 3) {
                studentRecord.enrolled = true;
            }
        }
    }

    private static void viewRegisteredStudents() {
        for (StudentRecord student : mainStudentRecord) {
            System.out.printf("Student Id: %s, Student Name: [%s], Status: %s\n",
                    student.id, student.name,
                    (student.enrolled) ? "Enrolled" : "Not Enrolled");
        }
    }

    private static void viewStudentRecords() {
        String inputStudentId = getInputStudentId();
        StudentRecord student = getStudentRecordById(inputStudentId);
        System.out.printf("Student Id: %s, Student Name: [%s], ", student.id, student.name);
        if (student.enrolled) {
            System.out.printf("Enrolled Subject: [%s]\n", getSubjectFromStudentRecord(student));
        } else {
            System.out.println("Not enrolled!");
        }
    }

    private static void viewCourse() {
        int inputCourseId = getInputCourse();
        System.out.printf("Subjects under [%s] Course = %s\n", COURSES.get(inputCourseId),
                SUBJECT_LIST.get(inputCourseId).toString());
    }

    private static void viewStatistics() {
        int totalEnrolledStudentsCounter = 0;
        int totalNonEnrolledStudentsCounter = 0;
        int[] registeredCourseCounter = new int[COURSES.size()];
        int[][] registeredSubjectPerCourseCounter = new int[COURSES.size()][MAX_SUBJECT_PER_COURSE];
        List<String> topCourses = new ArrayList<>();
        List<String> topSubjects = new ArrayList<>();
        class SubjectByCourse {
            int courseId;
            int subjectId;
        }

        // Get Count
        for (StudentRecord student : mainStudentRecord) {
            if (student.enrolled) {
                totalEnrolledStudentsCounter++;
            } else {
                totalNonEnrolledStudentsCounter++;
            }
            registeredCourseCounter[student.course]++;
            if (student.subject != null) {
                for (Integer subjectId : student.subject) {
                    registeredSubjectPerCourseCounter[student.course][subjectId]++;
                }
            }
        }

        // Get Top Courses
        List<Integer> topCoursesId = new ArrayList<>();
        int maxValue = 0;
        for (int courseIndex = 0; courseIndex < registeredCourseCounter.length; courseIndex++) {
            int count = registeredCourseCounter[courseIndex];
            if (count >= maxValue && count > 0) {
                if (count > maxValue) {
                    topCoursesId.clear();
                }
                topCoursesId.add(courseIndex);
                maxValue = count;
            }
        }
        for (int course : topCoursesId) {
            topCourses.add(COURSES.get(course));
        }

        // Get Top Subjects
        List<SubjectByCourse> topSubjectsId = new ArrayList<>();
        maxValue = 0;
        for (int courseIdx = 0; courseIdx < registeredSubjectPerCourseCounter.length; courseIdx++) {
            int[] subjectCounterList = registeredSubjectPerCourseCounter[courseIdx];
            for (int subjectIndex = 0; subjectIndex < subjectCounterList.length; subjectIndex++) {
                int count = subjectCounterList[subjectIndex];
                if (count >= maxValue && count > 0) {
                    if (count > maxValue) {
                        topSubjectsId.clear();
                    }
                    SubjectByCourse subjByCourse = new SubjectByCourse();
                    subjByCourse.courseId = courseIdx;
                    subjByCourse.subjectId = subjectIndex;
                    topSubjectsId.add(subjByCourse);
                    maxValue = count;
                }
            }
        }
        for (SubjectByCourse sbc : topSubjectsId) {
            topSubjects.add(SUBJECT_LIST.get(sbc.courseId).get(sbc.subjectId));
        }

        // print total # of enrolled students
        System.out.println("Total number of enrolled students = " + totalEnrolledStudentsCounter);

        // print total # of non-enrolled students
        System.out.println("Total number of non-enrolled students = " + totalNonEnrolledStudentsCounter);

        // print # of registered students per course
        System.out.println("Total number of registered students per course:");
        for (int index = 0; index < registeredCourseCounter.length; index++) {
            int regCtr = registeredCourseCounter[index];
            System.out.printf("Course [%s] = %d\n", COURSES.get(index), regCtr);
        }

        // print top enrolled course
        System.out.println("Top Course/s: " + topCourses);

        // print top enrolled subject
        System.out.println("Top Subjects/s: " + topSubjects);

    }

    private static void viewAllRecords() {
        for (StudentRecord rec : mainStudentRecord) {
            printStudentRecord(rec);
        }
    }

    private static void editStudentRecord() {
        System.out.println("Editing Student Record...");
        String inputStudentId = getInputStudentId();

        for (StudentRecord studentRecord : mainStudentRecord) {
            if (studentRecord.id.equals(inputStudentId)) {
                if (studentRecord.enrolled) {
                    System.out.println("ERROR: Unable to update Enrolled student!");
                    return;
                }

                System.out.print("Student Record to Edit: ");
                printStudentRecord(studentRecord);
                System.out.println("Select Student Information To Edit: [1=Name, " +
                        "2=Course, 3=Contact Number, 4=Subject]");
                System.out.print("Enter Info #: ");
                int recordIdToEdit = getUserInputIntToIndex();

                switch (recordIdToEdit) {
                    case 0:
                        editStudentName(studentRecord);
                        break;
                    case 1:
                        editStudentCourse(studentRecord);
                        break;
                    case 2:
                        editStudentContactNumber(studentRecord);
                        break;
                    case 3:
                        editStudentSubject(studentRecord);
                        break;
                }
                printUpdatedRecord(studentRecord);
            }
        }
    }

    private static void editStudentName(StudentRecord studentRecord) {
        System.out.println("Enter New Name: ");
        studentRecord.name = getInputName();
    }

    private static void editStudentCourse(StudentRecord student) {
        student.course = getInputCourse();
        System.out.println("WARNING! Changing of course will now delete all registered subjects!");
        student.subject.clear();
    }

    private static void editStudentContactNumber(StudentRecord studentRecord) {
        studentRecord.contactNumber = getInputContactNumber();
    }

    private static void editStudentSubject(StudentRecord student) {
        System.out.printf("Select Registered Subject To Edit: [%s]\n", getStudentSubjectListWithId(student));
        System.out.print("Enter Student Subject #: ");
        int registeredSubjectId = getUserInputIntToIndex();
        System.out.println("Select Subject To Replace: " + arrayToStringWithId(getSubjectListByCourse(student.course)));
        System.out.print("Enter Subject #: ");
        int newSubjectId = getUserInputIntToIndex();
        student.subject.set(registeredSubjectId, newSubjectId);
    }

    private static void deleteStudent() {
        String inputStudentId = getInputStudentId();
        mainStudentRecord.removeIf(student -> student.id.equals(inputStudentId));
        System.out.println("Student Id ["+ inputStudentId +"] is now removed in registry if exists");
    }

    private static void deleteSubject() {
        String inputStudentId = getInputStudentId();

        for (StudentRecord student : mainStudentRecord) {
            if (student.id.equals(inputStudentId)) {
                if (student.enrolled) {
                    System.out.println("ERROR: Unable to delete Subject of Enrolled student!");
                    return;
                }
                System.out.printf("Select Registered Subject To Delete: [%s]\n", getStudentSubjectListWithId(student));
                System.out.print("Enter Student Subject #: ");
                int registeredSubjectId = getUserInputIntToIndex();
                student.subject.remove(registeredSubjectId);
                return;
            }
        }

        System.out.printf("ERROR: Student Id [%s] not exist!\n", inputStudentId);
    }

    //*************** Authentication Method ***************//

    private static boolean authenticate() {
        boolean authorization = false;
        System.out.println("LOGIN:");
        System.out.print("Enter Username: ");
        String username = getUserInputString();
        System.out.print("Enter Password: ");
        String password = getUserInputString();

        if (username.equals(USERNAME) && password.equals(PASSWORD)) {
            authorization = true;
        }

        return authorization;
    }

    //*************** Printing Methods ***************//

    private static void printUpdatedRecord(StudentRecord rec) {
        System.out.print("Updated Record: ");
        printStudentRecord(rec);
    }

    private static void printStudentRecord(StudentRecord rec) {
        String subjects = getSubjectFromStudentRecord(rec);
        System.out.printf("[Id=%s, Name=[%s], ContactNumber=%s, Course=%s, Status=%s, Subject=[%s]]\n",
                rec.id, rec.name, rec.contactNumber, COURSES.get(rec.course),
                (rec.enrolled) ? "Enrolled" : "Not Enrolled", subjects);
    }

    //*************** User Input Methods ***************//

    private static int getInputSubjectId(int course) {
        System.out.println("Select Subject: " + arrayToStringWithId(SUBJECT_LIST.get(course)));
        System.out.print("Enter Subject #: ");
        return getUserInputIntToIndex();
    }

    private static String getInputStudentId() {
        System.out.print("Enter Student Id: ");
        return getUserInputString();
    }

    private static String getInputContactNumber() {
        System.out.print("Enter Contact Number: ");
        return getUserInputString();
    }

    private static int getInputCourse() {
        System.out.println("Select Course: " + arrayToStringWithId(COURSES));
        System.out.print("Enter Course #: ");
        return getUserInputIntToIndex();
    }

    private static String getInputName() {
        System.out.print("Enter Last Name: ");
        String lastName = getUserInputString();
        System.out.print("Enter First Name: ");
        String firstName = getUserInputString();
        System.out.print("Enter Middle Initial: ");
        String middleInitial = getUserInputString();
        return (lastName + ", " + firstName + ", " + middleInitial + ".").toUpperCase();
    }

    private static int getInputAddFunction() {
        System.out.printf("\nSelect Add Function: [%s]\n", arrayToStringWithId(FUNCTION_ADD));
        System.out.print("Enter Add Function #: ");
        return getUserInputIntToIndex();
    }

    private static int getInputViewFunction() {
        System.out.printf("\nSelect View Function: [%s]\n", arrayToStringWithId(FUNCTION_VIEW));
        System.out.print("Enter View Function #: ");
        return getUserInputIntToIndex();
    }

    private static int getInputDeleteFunction() {
        System.out.printf("\nSelect Delete Function: [%s]\n", arrayToStringWithId(FUNCTION_DELETE));
        System.out.print("Enter Delete Function #: ");
        return getUserInputIntToIndex();
    }

    private static int getInputFunction() {
        System.out.printf("\nSelect Function: [%s]\n", arrayToStringWithId(FUNCTION_MAIN));
        System.out.print("Enter Function #: ");
        return getUserInputIntToIndex();
    }

    private static int getUserInputIntToIndex() {
        String userInput = getUserInputString();
        return Integer.parseInt(userInput) - 1;
    }

    private static String getUserInputString() {
        Scanner reader = new Scanner(System.in);
        return reader.nextLine();
    }

    //*************** Get Data Methods ***************//

    private static StudentRecord getLastRecord() {
        if (mainStudentRecord.isEmpty()) {
            return null;
        }
        return mainStudentRecord.get(mainStudentRecord.size() - 1);
    }

    private static String getSubjectFromStudentRecord(StudentRecord student) {
        String subjects = "";
        if (student.subject != null) {
            StringBuilder stringBuilder = new StringBuilder();
            for (Integer subjectId : student.subject) {
                stringBuilder.append(SUBJECT_LIST.get(student.course).get(subjectId));
                if (student.subject.indexOf(subjectId) != student.subject.size() - 1) {
                    stringBuilder.append(", ");
                }
            }
            subjects = stringBuilder.toString();
        }
        return subjects;
    }

    private static StudentRecord getStudentRecordById(String studentId) {
        for (StudentRecord studentRecord : mainStudentRecord) {
            if (studentRecord.id.equals(studentId)) {
                return studentRecord;
            }
        }
        return null;
    }

    private static List<String> getSubjectListByCourse(int courseId) {
        switch (courseId) {
            case 0:
                return IT_SUBJECTS;
            case 1:
                return CS_SUBJECTS;
            case 2:
                return IS_SUBJECTS;
            case 3:
                return SE_SUBJECTS;
        }
        return null;
    }

    private static String getStudentSubjectListWithId(StudentRecord student) {
        return arrayToStringWithId(getStudentSubjectList(student));
    }

    private static List<String> getStudentSubjectList(StudentRecord student) {
        String subjects = getSubjectFromStudentRecord(student);
        return Arrays.asList(subjects.split(","));
    }

    //*************** Other Utilities Methods ***************//

    private static String generateId() {
        String id;
        int year = Calendar.getInstance().get(Calendar.YEAR);
        StudentRecord lastRec = getLastRecord();
        if (lastRec == null) {
            id = String.format("%4d%04d", year, 1);
        } else {
            int idSuffix = Integer.parseInt(lastRec.id.substring(lastRec.id.length() - 4)) + 1;
            id = String.format("%4d%04d", year, idSuffix);
        }
        return id;
    }

    // Transform array to string with id. e.g. "1=Add Student, 2=Add Subject"
    private static String arrayToStringWithId(List<String> array) {
        if(array == null || array.isEmpty()) {
            return "";
        }

        StringBuilder stringBuilder = new StringBuilder();
        int index = 1;
        for (String str : array) {
            stringBuilder.append(index++).append("=").append(str);
            if (array.indexOf(str) != array.size() - 1) {
                stringBuilder.append(", ");
            }
        }
        return stringBuilder.toString();
    }

    //*************** Defined Student Record Data Type ***************//
    private static class StudentRecord {
        private String id;
        private String name;
        private int course;
        private String contactNumber;
        private List<Integer> subject = new ArrayList<>();
        private boolean enrolled;
    }

}
